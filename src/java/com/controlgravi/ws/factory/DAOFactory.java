/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlgravi.ws.factory;

import com.controlgravi.ws.factory.contrato.EmpleadoDAO;
import com.controlgravi.ws.factory.contrato.UsuarioDAO;
import com.controlgravi.ws.factory.mysql.MySQLDAOFactory;

/**
 *
 * @author imjesr
 */
public abstract class DAOFactory {

    public enum Fuente {
        MY_SQL,
    }

    public abstract EmpleadoDAO getEmpleadoDAO();

    public abstract UsuarioDAO getUsuarioDAO();

    public static DAOFactory getDAOFactory(Fuente whichFactory) {
        switch (whichFactory) {
            case MY_SQL:
                return new MySQLDAOFactory();
            default:
                return null;
        }
    }
}
