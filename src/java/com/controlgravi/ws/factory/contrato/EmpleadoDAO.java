/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlgravi.ws.factory.contrato;

import java.util.List;

/**
 *
 * @author imjesr
 */
public interface EmpleadoDAO {

    public Object guardar(Object object);

    public Object eliminar(String id);

    public Object obtener(int id);

    public Object actualizar(Object object);

    public List obtenerTodos();
}
