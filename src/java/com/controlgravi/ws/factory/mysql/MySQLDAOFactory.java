/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlgravi.ws.factory.mysql;

import com.controlgravi.ws.factory.DAOFactory;
import com.controlgravi.ws.factory.contrato.EmpleadoDAO;
import com.controlgravi.ws.factory.contrato.UsuarioDAO;

/**
 *
 * @author imjesr
 */
public class MySQLDAOFactory extends DAOFactory {

    @Override
    public EmpleadoDAO getEmpleadoDAO() {
        return new MySQLEmpleadoDAO();
    }

    @Override
    public UsuarioDAO getUsuarioDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
